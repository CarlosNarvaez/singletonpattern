class NetworkResource constructor() {
    private val cacheManager: CacheManager = CacheManager.getInstance()
    fun doNetworkOperation() {
        println("Doing network operation")
        cacheManager.put("network", "this is some network operation")
        println("Network operation completed with hashcode *${cacheManager.hashCode()}*")
    }
}