class FileResource constructor() {
    private val cacheManager: CacheManager = CacheManager.getInstance()
    fun doFileOperation() {
        println("Doing file operation")
        cacheManager.put("file", "this is some file operation")
        println("File operation completed with hashcode *${cacheManager.hashCode()}*")
    }
}