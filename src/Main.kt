object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val cacheManager: CacheManager = CacheManager.getInstance()

        println("Initial CacheManager instance count is ${CacheManager.myInstancesCount}")

        println("")

        val networRes = NetworkResource()
        networRes.doNetworkOperation()
        println("CacheManager instance count is ${CacheManager.myInstancesCount} after network operation")

        println("")

        val fileRes = FileResource()
        fileRes.doFileOperation()
        println("CacheManager instance count is ${CacheManager.myInstancesCount} after file operation")

        println("")
        println("Main call cache manager hashcode *${cacheManager.hashCode()}*")

        println("File operation value: ${cacheManager.get("file")}")
        println("Network operation value: ${cacheManager.get("network")}")
        println("Main call cache manager hashcode after retrieving values *${cacheManager.hashCode()}*")

    }
}